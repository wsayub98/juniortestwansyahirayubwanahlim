<?php include('header.php');
error_reporting(E_ALL);

require_once 'classes/Product.php';
require_once 'classes/DVD.php';
require_once 'classes/Furniture.php';
require_once 'classes/Book.php';

$newProduct = new Product();
$newDVD = new DVD(false, $newProduct);
$newFurniture = new Furniture(false, false, false, $newProduct);
$newBook = new Book(false, $newProduct);

$dvds = $newDVD->getDvd();
$furnitures = $newFurniture->getFurniture();
$books = $newBook->getBook();

?>

<form action="delete.php" method="POST">
    <div class="row mt-3">
        <div class="col">
            <h3>Product List</h3>
        </div>
        <div class="col d-flex justify-content-end">
            <a href="addproduct" class="btn btn-primary">ADD</a>
            <input type="submit" class="mx-2 btn btn-danger" value="MASS DELETE" name="delete">

        </div>
    </div>
    <hr>
    <?php if (!empty($dvds)) : ?>
        <div class="row">
            <?php foreach ($dvds as $i => $dvd) : ?>
                <div class="my-4 col-sm-3">
                    <div class="card" style="width: 18rem;">
                        <div class="m-2 d-flex justify-content-start">
                            <input type="checkbox" name="dvds[]" value="<?= $dvd['product_id']; ?>" class="delete-checkbox form-check-input">
                        </div>
                        <div class="card-body">
                            <center>
                                <?= $dvd['product_sku']; ?><br>
                                <?= $dvd['product_name']; ?><br>
                                <?= $dvd['product_price']; ?> $<br>
                                Size: <?= $dvd['dvd_size']; ?> MB<br>
                            </center>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php if (!empty($books)) : ?>
        <div class="row">
            <?php foreach ($books as $i => $book) : ?>
                <div class="my-4 col-sm-3">
                    <div class="card" style="width: 18rem;">
                        <div class="m-2 d-flex justify-content-start">
                            <input type="checkbox" name="books[]" value="<?= $book['product_id']; ?>" class="delete-checkbox form-check-input">
                        </div>
                        <div class="card-body">
                            <center>
                                <?= $book['product_sku']; ?><br>
                                <?= $book['product_name']; ?><br>
                                <?= $book['product_price']; ?> $<br>
                                Weight: <?= $book['book_weight']; ?> KG<br>
                            </center>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php if (!empty($furnitures)) : ?>
        <div class="row">
            <?php foreach ($furnitures as $i => $furniture) : ?>
                <div class="my-4 col-sm-3">
                    <div class="card" style="width: 18rem;">
                        <div class="m-2 d-flex justify-content-start">
                            <input type="checkbox" name="furnis[]" value="<?= $furniture['product_id']; ?>" class="delete-checkbox form-check-input">
                        </div>
                        <div class="card-body">
                            <center>
                                <?= $furniture['product_sku']; ?><br>
                                <?= $furniture['product_name']; ?><br>
                                <?= $furniture['product_price']; ?> $<br>
                                Dimension: <?= $furniture['furni_height'] . 'x' . $furniture['furni_width'] . 'x' . $furniture['furni_length']; ?><br>
                            </center>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</form>

<?php include('footer.php'); ?>
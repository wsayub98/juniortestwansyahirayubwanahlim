<?php
require_once 'classes/Product.php';
require_once 'classes/DVD.php';
require_once 'classes/Furniture.php';
require_once 'classes/Book.php';

$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']
    === 'on' ? "https" : "http") . "://" .
    $_SERVER['HTTP_HOST'] . '/';

$newProduct = new Product();
$newDVD = new DVD(false, $newProduct);
$newFurniture = new Furniture(false, false, false, $newProduct);
$newBook = new Book(false, $newProduct);

//MASS DELETE START
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['delete'])) {
        if (isset($_POST['dvds'])) {
            $newDVD->todeleteDvd($_POST['dvds']);
        }
        if (isset($_POST['books'])) {
            $newBook->todeleteBook($_POST['books']);
        }
        if (isset($_POST['furnis'])) {
            $newFurniture->todeleteFurniture($_POST['furnis']);
        }
        header("Location: " . $link);
    }
}

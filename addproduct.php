<?php
require_once 'classes/Product.php';
require_once 'classes/DVD.php';
require_once 'classes/Furniture.php';
require_once 'classes/Book.php';
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $newProduct = new Product($_POST['product_sku'], $_POST['product_name'], $_POST['product_price'], $_POST['product_type']);
    if (isset($_POST['dvd_size'])) {
        $newDVD = new DVD($_POST['dvd_size'], $newProduct); //type = 1
        $newDVD->tosaveDvd();
    }
    if (isset($_POST['book_weight'])) {
        $newBook = new Book($_POST['book_weight'], $newProduct);
        $newBook->tosaveBook();
    }
    if (isset($_POST['furni_height'])) {
        $newFurniture = new Furniture($_POST['furni_height'], $_POST['furni_width'], $_POST['furni_length'], $newProduct);
        $newFurniture->tosaveFurniture();
    }

    exit();
}

?>

<?php include('header.php'); ?>
<form id="product_form" action="<?= $_SERVER['PHP_SELF']; ?>" method="POST">
    <div class="row my-3">
        <div class="col">
            <h3>Product Add</h3>
        </div>
        <div class="col d-flex justify-content-end">
            <button type="submit" name="save" class="btn btn-primary">Save</button>
            <a href="/" class="mx-2 btn btn-secondary">Cancel</a>
        </div>
    </div>
    <hr>
    <div class="row my-5">
        <div class="row mb-3">
            <label for="" class="col-sm-2 col-form-label">SKU</label>
            <div class="form-group col-sm-5">
                <input type="text" name="product_sku" class="form-control" id="sku" required>
            </div>
        </div>
        <div class="row mb-3">
            <label for="" class="col-sm-2 col-form-label">Name</label>
            <div class="form-group has-error col-sm-5">
                <input type="text" name="product_name" class="form-control required" id="name" required>
            </div>
        </div>
        <div class="row mb-3">
            <label for="" class="col-sm-2 col-form-label">Price ($)</label>
            <div class="form-group col-sm-5">
                <input type="number" min="1" step="any" name="product_price" class="form-control required" id="price" required>
            </div>
        </div>
        <div class="row mb-3">
            <label for="" class="col-sm-2 col-form-label">Type Switcher</label>
            <div class="form-group col-sm-5">
                <select name="product_type" id="productType" class="form-select required" aria-label="Default select example" required>
                    <option value="" selected>Type Switcher</option>
                    <option value=1 id="DVD">DVD</option>
                    <option value=3 id="Furniture">Furniture</option>
                    <option value=2 id="Book">Book</option>
                </select>
            </div>
        </div>

        <div class="row mb-3" id="productDetail">

        </div>
    </div>
</form>
<?php include('footer.php'); ?>
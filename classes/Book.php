<?php

class Book extends Product
{
    protected $table = 'books';
    protected $weight = 1;

    public function __construct($weight = false, $newProduct = false)
    {
        $this->weight = $weight;
        $this->productClass = $newProduct;
        $this->databaseClass = new Database;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function tosaveBook()
    {
        if (isset($this->weight)) {
            $product_id = $this->productClass->tosaveProduct();
        }

        $table = $this->table;
        $fields = [
            'book_weight' => $this->weight,
            'product_id' => $product_id,
        ];

        if (isset($fields['book_weight'])) {
            $res = $this->databaseClass->save($table, $fields);
            return $this->productClass->redirectIndex($res);
        }
    }

    public function getBook()
    {
        $productTable = $this->productClass->table;
        $bookTable = $this->table;

        $data = $this->databaseClass->join($productTable, $bookTable, 'types');

        return $data;
    }

    public function todeleteBook($product_id)
    {
        $productTable = $this->productClass->table;
        $bookTable = $this->table;

        foreach ($product_id as $productid) {
            $res = [
                $this->databaseClass->delete($bookTable, $productid),
                $this->databaseClass->delete($productTable, $productid)
            ];
        }

        if ($res) {
            return true;
        } else "Error";
    }
}

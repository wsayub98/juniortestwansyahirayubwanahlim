<?php

class Database
{
    private $host = 'localhost';
    private $username = 'id17301612_jrtestdb';
    private $password = 'e+_2nno0l$yk/s\Q';
    private $dbName = 'id17301612_juniortest';

    protected $db;

    public function __construct()
    {
        $this->dbConnection();
    }

    public function dbConnection()
    {
        try {
            $this->db = new mysqli($this->host, $this->username, $this->password, $this->dbName);
        } catch (Exception $e) {
            echo 'Error Message: ' . $e->getMessage();
        }
    }

    public function save($table, $fields = [''])
    {
        $insert = "INSERT INTO " . $table . " (" . implode(",", array_keys($fields)) . ")VALUES('" . implode("','", array_values($fields)) . "')";

        $result = $this->db->query($insert);
        // var_dump($insert);

        if ($result) {
            return $this->db->insert_id;
        } else return false;
    }

    public function join($productTable = 'products', $bdfTable = '', $type = 'types') //product,book,type
    {
        // SELECT * FROM (`products` INNER JOIN `books` ON products.id = books.product_id) INNER JOIN `types` ON products.ptype_id = types.type_id
        $select = "SELECT * FROM ({$productTable} INNER JOIN {$bdfTable} ON {$productTable}.product_id = {$bdfTable}.product_id) INNER JOIN {$type} ON {$productTable}.ptype_id = {$type}.type_id";
        $result = $this->db->query($select);

        if ($result) {
            return $result->fetch_all(MYSQLI_ASSOC);
        } else return false;
    }

    public function delete($table, $product_id)
    {
        $delete = "DELETE FROM {$table} WHERE product_id = {$product_id}";
        $result = $this->db->query($delete);

        if (!$result) echo "ERROR DELETING";
        else return true;
    }
}

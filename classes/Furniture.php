<?php

class Furniture extends Product
{
    protected $table = 'furnitures';
    protected $height = 1;
    protected $width = 1;
    protected $length = 1;

    public function __construct($height = false, $width = false, $length = false, $newProduct = false)
    {
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
        $this->productClass = $newProduct;
        $this->databaseClass = new Database;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getHeight()
    {
        return $this->height;
    }
    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function getWidth()
    {
        return $this->width;
    }
    public function setLength($length)
    {
        $this->length = $length;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function tosaveFurniture()
    {
        if (isset($this->length)) {
            $product_id = $this->productClass->tosaveProduct();
        }

        $table = $this->table;
        $fields = [
            'furni_height' => $this->height,
            'furni_width' => $this->width,
            'furni_length' => $this->length,
            'product_id' => $product_id,
        ];

        if (isset($fields['furni_length'])) {
            $res = $this->databaseClass->save($table, $fields);
            return $this->productClass->redirectIndex($res);
        }
    }

    public function getFurniture()
    {
        $productTable = $this->productClass->table;
        $furnitureTable = $this->table;

        $data = $this->databaseClass->join($productTable, $furnitureTable, 'types');

        return $data;
    }

    public function todeleteFurniture($product_id)
    {
        $productTable = $this->productClass->table;
        $furnitureTable = $this->table;

        foreach ($product_id as $productid) {
            $res = [
                $this->databaseClass->delete($furnitureTable, $productid),
                $this->databaseClass->delete($productTable, $productid)
            ];
        }

        if ($res) {
            return true;
        } else "Error";
    }
}

<?php

class DVD extends Product
{
    protected $table = 'dvds';
    protected $size;

    public function __construct($size = false, $newProduct = false)
    {
        $this->size = $size;
        $this->productClass = $newProduct;
        $this->databaseClass = new Database;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function tosaveDvd()
    {
        if (isset($this->size)) {
            $product_id = $this->productClass->tosaveProduct();
        }

        $table = $this->table;
        $fields = [
            'dvd_size' => $this->size,
            'product_id' => $product_id,
        ];

        if (isset($fields['dvd_size'])) {
            $res = $this->databaseClass->save($table, $fields);
            return $this->productClass->redirectIndex($res);
        }
    }

    public function getDvd()
    {
        $productTable = $this->productClass->table;
        $dvdTable = $this->table;

        $data = $this->databaseClass->join($productTable, $dvdTable, 'types');

        return $data;
    }

    public function todeleteDvd($product_id)
    {
        $productTable = $this->productClass->table;
        $dvdTable = $this->table;

        foreach ($product_id as $productid) {
            $res = [
                $this->databaseClass->delete($dvdTable, $productid),
                $this->databaseClass->delete($productTable, $productid)
            ];
        }

        if ($res) {
            return true;
        } else "Error";
    }
}

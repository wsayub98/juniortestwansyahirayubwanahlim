<?php
require_once 'Database.php';

class Product
{
    protected $table = 'products';
    public $product_sku;
    public $product_name;
    public $product_price;
    public $product_type;

    public function __construct($product_sku = false, $product_name = false, $product_price = false, $product_type = false)
    {
        $this->product_sku = $product_sku;
        $this->product_name = $product_name;
        $this->product_price = $product_price;
        $this->product_type = $product_type;
        $this->databaseClass = new Database;
    }

    public function tosaveProduct()
    {
        $table = $this->table;
        $fields = [
            'product_sku' => $this->product_sku,
            'product_name' => $this->product_name,
            'product_price' => $this->product_price,
            'ptype_id' => $this->product_type,
        ];
        $res = $this->databaseClass->save($table, $fields); // this function return inserted 'product_id'
        // var_dump($res);

        return $res;
    }

    public function redirectIndex($res, $d = true)
    {
        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']
            === 'on' ? "https" : "http") . "://" .
            $_SERVER['HTTP_HOST'] . '/';
// juniortest.wansyahirayub.wanahlim/index.php
        if ($res) {
            return header("Location: " . $link);
        } else echo 'ERROR RESULT FAILED.';
    }
}

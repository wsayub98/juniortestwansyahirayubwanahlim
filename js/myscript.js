$(document).ready(function () {
    $('#productType').change(function () {
        var productType = this.value;
        // alert(this.value);  
        if (productType == 2) //book min="1" step="any" 
        {
            $('#productDetail').append(
                $('#productDetail').empty(),
                '<div class="row mb-3">',
                '<span class="text-secondary"><i>Please, provide weight.</i></span><br>',
                '<label for="" class="col-sm-2 col-form-label">Weight (KG)</label>',
                '<div class="form-group col-sm-5"><input type="number" class="form-control" name="book_weight" id="weight" required>',
                '</div>',

            )
        }
        else if (productType == 3) {
            // console.log('Furn');
            $('#productDetail').append(
                $('#productDetail').empty(),
                '<div class="row mb-3">',
                '<span class="text-secondary"><i>Please, provide dimensions.</i></span><br>',
                '<label for="" class="col-sm-2 col-form-label">Height (CM)</label>',
                '<div class="form-group col-sm-5"><input type="number" class="form-control" name="furni_height" id="height" required>',
                '</div>',
                ' </div>',
                '<div class="row mb-3">',
                '<label for="" class="col-sm-2 col-form-label">Width (CM)</label>',
                '<div class="form-group col-sm-5"><input type="number" class="form-control" name="furni_width" id="width" required>',
                '</div>',
                ' </div>',
                '<div class="row mb-3">',
                '<label for="" class="col-sm-2 col-form-label">Length (CM)</label>',
                '<div class="form-group col-sm-5"><input type="number" class="form-control" name="furni_length" id="length" required>',
                '</div>',
                ' </div>',
            )
        }
        else if (productType == 1) {
            // console.log('Dvd');
            $('#productDetail').append(
                $('#productDetail').empty(),
                '<div class="row mb-3">',
                '<span class="text-secondary"><i>Please, provide size.</i></span><br>',
                '<label for="" class="col-sm-2 col-form-label">Size (MB)</label>',
                '<div class="form-group col-sm-5"><input type="number" min="1" step="any" class="form-control" name="dvd_size" id="size" required>',
                '</div>',
                ' </div>'
            )
        } else {
            $('#productDetail').append(
                $('#productDetail').empty(),
            )
        }
    })

    $.validator.setDefaults({
        errorClass: 'invalid-feedback',
        highlight: function (element) {
            $(element)
                .closest('.form-control')
                .addClass(' is-invalid')
        },
        unhighlight: function (element) {
            $(element)
                .closest('.form-control')
                .removeClass(' is-invalid')
        }
    })

    $.validator.addMethod("nowhitespace", function (value, element) {
        return this.optional(element) || /^\S+$/i.test(value);
    }, "Please, do not use space for this input.");

    $("#product_form").validate({
        rules: {
            product_sku: {
                nowhitespace: true,
            },
        },
        messages: {
            product_sku: {
                required: "Please, submit required data.",
            },
            product_name: {
                required: "Please, submit required data.",
            },
            product_price: {
                required: "Please, submit required data.",
            },
            product_type: {
                required: "Please, submit required data.",
            },
            book_weight: { required: "Please, submit required data.", },
            dvd_size: { required: "Please, submit required data.", },
            furni_length: { required: "Please, submit required data.", },
            furni_width: { required: "Please, submit required data.", },
            furni_height: { required: "Please, submit required data.", },
        },

        submitHandler: function (form) {
            form.submit();
        }
    });
})

